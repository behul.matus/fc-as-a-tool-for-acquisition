def likvidita_1(krat_fin_maj, krat_zav, krat_bank_uver, krat_fin_vyp) :
    """
    krat_fin_maj = Kratkodoby financny majetok \n
    krat_zav = Kratkodobe zavazky \n
    krat_bank_uver = Kratkodobé bankove uvery \n
    krat_fin_vyp = Kratkodobe financne vypomoci) \n
    return krat_fin_maj / (krat_zav + krat_bank_uver + krat_fin_vyp)
    """
    return krat_fin_maj / (krat_zav + krat_bank_uver + krat_fin_vyp)

def likvidita_2(krat_pohl, krat_fin_maj, krat_zav, krat_bank_uver, krat_fin_vyp) :
    """
    krat_pohl = Kratkodobe pohladavky \n
    krat_fin_maj = Kratkodoby financny majetok \n
    krat_zav = Kratkodobe zavazky \n
    krat_bank_uver = Kratkodobé bankove uvery \n
    krat_fin_vyp = Kratkodobe financne vypomoci) \n
    return (krat_pohl + krat_fin_maj) / (krat_zav + krat_bank_uver + krat_fin_vyp)
    """
    return (krat_pohl + krat_fin_maj) / (krat_zav + krat_bank_uver + krat_fin_vyp)

def likvidita_3(zasoby, krat_pohl, krat_fin_maj, krat_zav, krat_bank_uver, krat_fin_vyp) :
    """
    zasoby = Zasoby \n
    krat_pohl = Kratkodobe pohladavky \n
    krat_fin_maj = Kratkodoby financny majetok \n
    krat_zav = Kratkodobe zavazky \n
    krat_bank_uver = Kratkodobé bankove uvery \n
    krat_fin_vyp = Kratkodobe financne vypomoci) \n
    return (zasoby + krat_pohl + krat_fin_maj) / (krat_zav + krat_bank_uver + krat_fin_vyp)
    """
    return (zasoby + krat_pohl + krat_fin_maj) / (krat_zav + krat_bank_uver + krat_fin_vyp)

def stupen_samofinancovania(vlast_im, aktiva) :
    """
    vlast_im = Vlastne imanie \n
    aktiva = Aktiva \n
    return vlast_im / aktiva
    """
    return vlast_im / aktiva

def uverova_zadlzenost_aktiv(bank_uver, aktiva) :
    """
    bank_uver = Bankove uvery \n
    aktiva = Aktiva \n
    return bank_uver / aktiva
    """
    return bank_uver / aktiva

def rentabilita_aktiv(eat, aktiva) :
    """
    eat = Earnings Before Tax (VH za UO pred zdanenim) \n
    aktiva = Aktiva \n
    return eat / aktiva
    """
    return eat / aktiva

def rentabilita_vlastneho_kapitalu(eat, vlast_im) :
    """
    eat = Earnings Before Tax (VH za UO pred zdanenim) \n
    vlast_im = Vlastne imanie \n
    return eat / vlast_im
    """
    return eat / vlast_im

def rentabilita_trzieb(eat, trzby) :
    """
    eat = Earnings Before Tax (VH za UO pred zdanenim) \n
    trzby = Trzby (not from selling material and assets) \n
    return eat / trzby
    """
    return eat / trzby

def pridana_hodnota_na_trzbach(prid_hod, trzby) :
    """
    prid_hod = Pridana hodnota \n
    trzby = Trzby (not from selling material and assets) \n
    return prid_hod / trzby
    """
    return prid_hod / trzby

def doba_obratu_zasob(zasoby, vynos_hc, days) :
    """
    zasoby = Zasoby \n
    vynos_hc = Vynosy z hospodarskej cinnosti \n
    days = 360 or 365 based on wanted period \n
    return (zasoby / vynos_hc) * days
    """
    return (zasoby / vynos_hc) * days

def doba_inkasa_pohladavok(pohlad, vynos_hc, days) :
    """
    pohlad = Pohladavky \n
    vynos_hc = Vynosy z hospodarskej cinnosti \n
    days = 360 or 365 based on wanted period \n
    return (pohlad / vynos_hc) * days
    """
    return (pohlad / vynos_hc) * days

def doba_splatnosti_zavazkov(zavazky, vynos_hc, days) :
    """
    zavazky = Zavazky \n
    vynos_hc = Vynosy z hospodarskej cinnosti \n
    days = 360 or 365 based on wanted period \n
    return (zavazky / vynos_hc) * days
    """
    return (zavazky / vynos_hc) * days

def obrat_aktiv(trzby, aktiva) :
    """
    trzby = Trzby (not from selling material and assets) \n
    aktiva = Aktiva \n
    return trzby / aktiva
    """
    return trzby / aktiva

def prevadzkova_nakladovost(nakl_hc, vynos_hc) :
    """
    nakl_hc = Naklady na hospodarsku cinnost \n
    vynos_hc = Vynosy z hospodarskej cinnosti \n
    return nakl_hc / vynos_hc
    """
    return nakl_hc / vynos_hc

def materialova_nakladovost(spotreba, vynos_hc) :
    """
    spotreba = Spotreba materiálu a energie \n
    vynos_hc = Vynosy z hospodarskej cinnosti \n
    return spotreba / vynos_hc
    """
    return spotreba / vynos_hc

def cisty_pracovny_kapital_na_aktivach(cpk, aktiva) :
    """
    cpk = CPK (dlhodobe zavazky + vlastne imanie - neobezny majetok) \n
    aktiva = Aktiva \n
    return cpk / aktiva
    """
    return cpk / aktiva

def nerozdeleny_zisk_na_aktivach(ner_zisk, aktiva) :
    """
    ner_zisk = Nerozdeleny zisk/strata z minulych obdobi \n
    aktiva = Aktiva \n
    return ner_zisk / aktiva
    """
    return ner_zisk / aktiva

def ebit_na_aktivach(ebit, aktiva) :
    """
    ebit = Earnings before interests and tax \n
    aktiva = Aktiva \n
    return ebit / aktiva
    """
    return ebit / aktiva

def vlastne_imanie_na_zavazkoch(vlast_im, zavazky) :
    """
    vlast_im = Vlastne imanie \n
    zavazky = Zavazky \n
    return vlast_im / zavazky
    """
    return vlast_im / zavazky

def trzby_na_aktivach(trzby, aktiva) :
    """
    trzby = Trzby (not from selling material and assets) \n
    aktiva = Aktiva \n
    return trzby / aktiva
    """
    return trzby / aktiva

def ebit_na_kratkodobych_zavazkoch(ebit, krat_zav) :
    """
    ebit = Earnings before interests and tax \n
    krat_zav = Kratkodobe zavazky \n
    return trzby / aktiva
    """
    return ebit / krat_zav

def aktiva_na_zavazkoch(aktiva, zavazky) : 
    """
    aktiva = Aktiva \n
    zavazky = Zavazky \n
    return aktiva / zavazky
    """
    return aktiva / zavazky

def ebit_na_trzbach(ebit, trzby) : 
    """
    ebit = Earnings before interests and tax \n
    trzby = Trzby \n
    return ebit / trzby
    """
    return ebit / trzby

def zasoby_na_trzbach(zasoby, trzby) : 
    """
    zasoby = Zasoby \n
    trzby = Trzby \n
    return zasoby / trzby
    """
    return zasoby / trzby

def cash_flow_I(eat, odpisy) : 
    """
    eat = Earnings After Tax (VH za UO po zdaneni) \n
    odpisy = Odpisy a opravne polozky \n
    return eat + odpisy
    """
    return eat + odpisy

def cash_flow_II(cfI, zasoby_0, zasoby_1) : 
    """
    cfI = Cash FLow I (EAT + Odpisy) \n
    zasoby_0 = Zasoby za toto obdobie \n
    zasoby_1 = Zasoby za minule obdobie \n
    return cfI + (zasoby_1 - zasoby_0)
    """
    return cfI + (zasoby_1 - zasoby_0)

def cash_flow_III(cfII, pohl_0, pohl_1, krat_zav_0, krat_zav_1) : 
    """
    cfII =  \n
    pohl_0 = Pohladavky za toto obdobie \n
    pohl_1 = Pohladavky za minule obdobie \n
    krat_zav_0 = Kratkodobe zavazky za toto obdobie \n
    krat_zav_1 = Kratkodobe zavazky za minule obdobie \n
    return cfII + (pohl_1 - pohl_0) + (krat_zav_0 - krat_zav_1)
    """
    return cfII + (pohl_1 - pohl_0) + (krat_zav_0 - krat_zav_1)

def cash_flow_na_vynosoch(cfI, vynos_hc, vynos_fc) : 
    """
    cfI = Cash flow I (EAT + Odpisy) \n
    vynos_hc = Vynosy z hospodarskej cinnosti \n
    vynos_fc = Vynosy z financnej cinnosti \n
    return cfI / (vynos_hc + vynos_fc)
    """
    return cfI / (vynos_hc + vynos_fc)

def cash_flow_na_zavazkoch(cfI, zavazky) :
    """
    cfI = Cash FLow I (EAT + Odpisy) \n
    zavazky = Zavazky \n
    return cfI / zavazky
    """
    return cfI / zavazky

def navratnost_cudzieho_kapitalu(zavazky, fin_maj, cfI) :
    """
    fin_maj = Financny majetok + Financne ucty \n
    cfI = Cash FLow I (EAT + Odpisy) \n
    zavazky = Zavazky \n
    return (zavazky - fin_maj) / cfI
    """
    return (zavazky - fin_maj) / cfI

def tokove_zadlzenie(zavazky, cfI) :
    """
    zasoby = Zasoby \n
    cfI = Cash FLow I (EAT + Odpisy) \n
    return zavazky / cfI
    """
    return zavazky / cfI

def kralickov_rychly_test(stupen_samofinancovania, navratnost_cudzieho_kapitalu, cash_flow_na_vynosoch, rentabilita_aktiv) :
    """
    
    """
    scoring = {
        'stupen_samofinancovania' : {
            -1000 : 5,
            0.05 : 4,
            0.1 : 3,
            0.2 : 2,
            0.3 : 1
        },
        'navratnost_cudzieho_kapitalu' : {
            1000 : 5,
            30 : 4,
            12 : 3,
            5 : 2,
            3 : 1
        },
        'cash_flow_na_vynosoch' : {
            -1000 : 5,
            0.02 : 4,
            0.05 : 3,
            0.08 : 2,
            0.1 : 1
        },
        'rentabilita_aktiv' : {
            -1000 : 5,
            0.04 : 4,
            0.08 : 3,
            0.12 : 2,
            0.15 : 1
        }
    }
    sc1 = 0
    for target in scoring['stupen_samofinancovania'] :
         if stupen_samofinancovania > target : sc1 = scoring['stupen_samofinancovania'][target]
    sc2 = 0
    for target in scoring['navratnost_cudzieho_kapitalu'] :
         if navratnost_cudzieho_kapitalu < target : sc2 = scoring['navratnost_cudzieho_kapitalu'][target]
    sc3 = 0
    for target in scoring['cash_flow_na_vynosoch'] :
         if cash_flow_na_vynosoch > target : sc3 = scoring['cash_flow_na_vynosoch'][target]
    sc4 = 0
    for target in scoring['rentabilita_aktiv'] :
         if rentabilita_aktiv > target : sc4 = scoring['rentabilita_aktiv'][target]

    # return {
    #     "Stupeň samofinancovania" : sc1, 
    #     "Návratnosť cudzieho kapitálu" : sc2, 
    #     "Cash Flow na výnosoch" : sc3, 
    #     "Rentabilita aktív" : sc4
    # }
    return sc1 + sc2 + sc3 + sc4

def springate_model(cisty_pracovny_kapital_na_aktivach, ebit_na_aktivach, ebit_na_kratkodobych_zavazkoch, trzby_na_aktivach) :
    """
    1.03 x cisty_pracovny_kapital_na_aktivach => Cisty pracovny kapital / Aktiva \n
    3.07 x ebit_na_aktivach => EBIT / Aktiva \n
    0.66 x ebit_na_kratkodobych_zavazkoch => EBIT / Kratkodobe zavazky \n
    0.40 x trzby_na_aktivach => Trzby / Aktiva \n
    return (1.03 * cisty_pracovny_kapital_na_aktivach) + (3.07 * ebit_na_aktivach) + (0.66 * ebit_na_kratkodobych_zavazkoch) + (0.4 * trzby_na_aktivach)
    """
    return (1.03 * cisty_pracovny_kapital_na_aktivach) + (3.07 * ebit_na_aktivach) + (0.66 * ebit_na_kratkodobych_zavazkoch) + (0.4 * trzby_na_aktivach)

def altmanovo_z_score(cisty_pracovny_kapital_na_aktivach, nerozdeleny_zisk_na_aktivach, ebit_na_aktivach, vlastne_imanie_na_zavazkoch, trzby_na_aktivach) :
    """
    1.2 x cisty_pracovny_kapital_na_aktivach => Cisty pracovny kapital / Aktiva \n
    1.4 x nerozdeleny_zisk_na_aktivach => Vysledok hosp. minulych obdobi / Aktiva \n
    3.3 x ebit_na_aktivach => EBIT / Aktiva \n
    0.6 x vlastne_imanie_na_zavazkoch => Trhova hodnota / Zavazky \n
    1.0 x trzby_na_aktivach => Trzby / Aktiva \n
    return (1.2 * cisty_pracovny_kapital_na_aktivach) + (1.4 * nerozdeleny_zisk_na_aktivach) + (3.3 * ebit_na_aktivach) + (0.6 * vlastne_imanie_na_zavazkoch) + (1.0 * trzby_na_aktivach)
    """
    return (1.2 * cisty_pracovny_kapital_na_aktivach) + (1.4 * nerozdeleny_zisk_na_aktivach) + (3.3 * ebit_na_aktivach) + (0.6 * vlastne_imanie_na_zavazkoch) + (1.0 * trzby_na_aktivach)

def index_bonity(cash_flow_na_zavazkoch, aktiva_na_zavazkoch, ebit_na_aktivach, ebit_na_trzbach, zasoby_na_trzbach, trzby_na_aktivach) :
    """
    1.50 x cash_flow_na_zavazkoch => Cash Flow I / Zavazky \n
    0.08 x aktiva_na_zavazkoch => Aktiva / Zavazky \n
    10.0 x ebit_na_aktivach => EBIT / Aktiva \n
    5.00 x ebit_na_trzbach => EBIT / Trzby \n
    0.30 x zasoby_na_trzbach => Zasoby / Trzby \n
    0.10 x trzby_na_aktivach => Trzby / Aktiva \n
    return (1.5 *cash_flow_na_zavazkoch) + (0.08 * aktiva_na_zavazkoch) + (10 * ebit_na_aktivach) + (5 * ebit_na_trzbach) + (0.3 * zasoby_na_trzbach) + (0.1 * trzby_na_aktivach)
    """
    return (1.5 *cash_flow_na_zavazkoch) + (0.08 * aktiva_na_zavazkoch) + (10 * ebit_na_aktivach) + (5 * ebit_na_trzbach) + (0.3 * zasoby_na_trzbach) + (0.1 * trzby_na_aktivach)

def zlate_bilancne_pravidlo(dlhod_zav, dlhod_maj, vlast_im) :
    """
    vlast_im = Vlastne imanie \n
    dlhod_zav = Dlhodobe zavazky \n
    dlhod_maj = Dlhodoby majetok \n
    return (vlast_im + dlhod_zav) / dlhod_maj
    """
    return (vlast_im + dlhod_zav) / dlhod_maj


def all_indicators() :
    return [
        'navratnost_cudzieho_kapitalu',
        'likvidita_1',
        'likvidita_2',
        'likvidita_3',
        'stupen_samofinancovania',
        'uverova_zadlzenost_aktiv',
        'tokove_zadlzenie',
        'rentabilita_aktiv',
        'rentabilita_vlastneho_kapitalu',
        'rentabilita_trzieb',
        'pridana_hodnota_na_trzbach',
        'doba_obratu_zasob',
        'doba_inkasa_pohladavok',
        'doba_splatnosti_zavazkov',
        'obrat_aktiv',
        'prevadzkova_nakladovost',
        'materialova_nakladovost',
        'cisty_pracovny_kapital_na_aktivach',
        'nerozdeleny_zisk_na_aktivach',
        'ebit_na_aktivach',
        'vlastne_imanie_na_zavazkoch',
        'trzby_na_aktivach',
        'ebit_na_kratkodobych_zavazkoch',
        'aktiva_na_zavazkoch',
        'ebit_na_trzbach',
        'zasoby_na_trzbach',
        'cash_flow_I',
        'cash_flow_II',
        'cash_flow_III',
        'cash_flow_na_vynosoch',
        'cash_flow_na_zavazkoch',
        'kralickov_rychly_test',
        'springate_model',
        'altmanovo_z_score',
        'index_bonity',
        'zlate_bilancne_pravidlo',
    ]

indicators_structure = {
    'navratnost_cudzieho_kapitalu' : 0,
    'likvidita_1' : 0,
    'likvidita_2' : 0,
    'likvidita_3' : 0,
    'stupen_samofinancovania' : 0,
    'uverova_zadlzenost_aktiv' : 0,
    'tokove_zadlzenie' : 0,
    'rentabilita_aktiv' : 0,
    'rentabilita_vlastneho_kapitalu' : 0,
    'rentabilita_trzieb' : 0,
    'pridana_hodnota_na_trzbach' : 0,
    'doba_obratu_zasob' : 0,
    'doba_inkasa_pohladavok' : 0,
    'doba_splatnosti_zavazkov' : 0,
    'obrat_aktiv' : 0,
    'prevadzkova_nakladovost' : 0,
    'materialova_nakladovost' : 0,
    'cisty_pracovny_kapital_na_aktivach' : 0,
    'nerozdeleny_zisk_na_aktivach' : 0,
    'ebit_na_aktivach' : 0,
    'vlastne_imanie_na_zavazkoch' : 0,
    'trzby_na_aktivach' : 0,
    'ebit_na_kratkodobych_zavazkoch' : 0,
    'aktiva_na_zavazkoch' : 0,
    'ebit_na_trzbach' : 0,
    'zasoby_na_trzbach' : 0,
    'cash_flow_I' : 0,
    'cash_flow_II' : 0,
    'cash_flow_III' : 0,
    'cash_flow_na_vynosoch' : 0,
    'cash_flow_na_zavazkoch' : 0,
    'kralickov_rychly_test' : 0,
    'springate_model' : 0,
    'altmanovo_z_score' : 0,
    'index_bonity' : 0,
    'zlate_bilancne_pravidlo' : 0
}

data_structure = {
    699 : {
        'Strana aktív' : {
            'kratkodoby_fin_maj' : 'Krátkodobý finančný majetok súčet',
            'zasoby' : 'Zásoby súčet',
            'aktiva' : 'SPOLU MAJETOK',
            'dlhodobe_pohladavky' : 'Dlhodobé pohľadávky súčet',
            'neobezny_majetok' : 'Neobežný majetok',
            'kratkodobe_pohladavky' : 'Krátkodobé pohľadávky súčet',
            'financne_ucty' : 'Finančné účty',
        },
        'Strana pasív' : {
            'kratkodobe_zavazky' : 'Krátkodobé záväzky súčet',
            'kratkodobe_bank_uv' : 'Bežné bankové úvery',
            'kratkodobe_fin_vyp' : 'Krátkodobé finančné výpomoci',
            'vlastne_imanie' : 'Vlastné imanie',
            'zavazky' : 'Záväzky',
            'dlhodobe_zavazky' : 'Dlhodobé záväzky súčet',
            'dlhodobe_bank_uv' : 'Dlhodobé bankové úvery',
            'nerozdeleny_zisk' : 'Výsledok hospodárenia minulých rokov',
        },
        'Výkaz ziskov a strát' : {
            'eat' : 'Výsledok hospodárenia za účtovné obdobie po zdanení',
            'odpisy' : 'Odpisy a opravné položky k dlhodobému nehmotnému majetku a dlhodobému hmotnému majetku',
            'pridana_hodnota' : 'Pridaná hodnota',
            'nahlady_na_hc' : 'Náklady na hospodársku činnosť spolu',
            'vynosy_z_hc' : 'Výnosy z hospodárskej činnosti spolu súčet',
            'spotreba_mae' : 'Spotreba materiálu, energie a ostatných neskladovateľných dodávok',
            'ebt' : 'Výsledok hospodárenia za účtovné obdobie pred zdanením',
            'vynosy_z_fc' : 'Výnosy z finančnej činnosti spolu',
            'trzby_z_predaja_cp' : None,
            'predane_cenne_papiere' : None,
            'vynosy_z_dlhod_fin_maj' : None,
            'vynosy_z_kratk_fin_maj' : None,
            'vynosy_z_precenenia_cp' : None,
            'vynosove_uroky' : None,
            'kurzove_zisky' : None,
            'ostatne_vynosy_z_fc' : None,
            'trzby_za_tovar' : 'Tržby z predaja tovaru',
            'trzby_za_vyrobky' : 'Tržby z predaja vlastných výrobkov',
            'trzby_za_sluzby' : 'Tržby z predaja služieb',
            'obchodna_marza' : None,
            'zmena_stavu_vo_zasob' : 'Zmeny stavu vnútroorganizačných zásob',
            'aktivacia' : 'Aktivácia',
            'naklady_na_predany_tovar' : 'Náklady vynaložené na obstaranie predaného tovaru',
            'nakladove_uroky' : 'Nákladové úroky',
            'vyrobna_spotreba' : None,
            'vyroba' : None,
            'sluzby' : 'Služby',
            'trzby_z_predaja_dm_a_mat' : None,
            'ostatne_vynosy_z_hc' : None,
            'vysledok_hosp_z_hc' : 'Výsledok hospodárenia z hospodárskej činnosti',
        },
    },
    687 : {
        'Strana aktív' : {
            'kratkodoby_fin_maj' : 'Finančný majetok',
            'zasoby' : 'Zásoby',
            'aktiva' : 'SPOLU MAJETOK',
            'dlhodobe_pohladavky' : 'Dlhodobé pohľadávky',
            'neobezny_majetok' : 'Neobežný majetok',
            'kratkodobe_pohladavky' : 'Krátkodobé pohľadávky súčet',
            'financne_ucty' : None,
        },
        'Strana pasív' : {
            'kratkodobe_zavazky' : 'Krátkodobé záväzky okrem rezerv, úverov a výpomoci súčet',
            'kratkodobe_bank_uv' : 'Bežné bankové úvery',
            'kratkodobe_fin_vyp' : 'Krátkodobé finančné výpomoci',
            'vlastne_imanie' : 'Vlastné imanie',
            'zavazky' : 'Záväzky',
            'dlhodobe_zavazky' : 'Dlhodobé záväzky okrem rezerv a úverov',
            'dlhodobe_bank_uv' : 'Dlhodobé bankové úvery',
            'nerozdeleny_zisk' : 'Nerozdelený zisk alebo neuhradená strata minulých rokov',
        },
        'Výkaz ziskov a strát' : {
            'eat' : 'Výsledok hospodárenia za účtovné obdobie po zdanení',
            'odpisy' : 'Odpisy a opravné položky k dlhodobému nehmotnému majetku a dlhodobému hmotnému majetku',
            'pridana_hodnota' : 'Pridaná hodnota',
            'nahlady_na_hc' : 'Náklady na hospodársku činnosť spolu súčet',
            'vynosy_z_hc' : 'Výnosy z hospodárskej činnosti spolu súčet',
            'spotreba_mae' : 'Spotreba materiálu, energie a ostatných neskladovateľných dodávok',
            'ebt' : 'Výsledok hospodárenia za účtovné obdobie pred zdanením',
            'vynosy_z_fc' : 'Výnosy z finančnej činnosti spolu súčet',
            'trzby_z_predaja_cp' : None,
            'predane_cenne_papiere' : None,
            'vynosy_z_dlhod_fin_maj' : None,
            'vynosy_z_kratk_fin_maj' : None,
            'vynosy_z_precenenia_cp' : None,
            'vynosove_uroky' : None,
            'kurzove_zisky' : None,
            'ostatne_vynosy_z_fc' : None,
            'trzby_za_tovar' : 'Tržby z predaja tovaru',
            'trzby_za_vyrobky' : 'Tržby z predaja vlastných výrobkov a služieb',
            'trzby_za_sluzby' : None,
            'obchodna_marza' : None,
            'zmena_stavu_vo_zasob' : 'Zmena stavu vnútroorganizačných zásob',
            'aktivacia' : 'Aktivácia',
            'naklady_na_predany_tovar' : 'Náklady vynaložené na obstaranie predaného tovaru',
            'nakladove_uroky' : 'Nákladové úroky',
            'vyrobna_spotreba' : None,
            'vyroba' : None,
            'sluzby' : 'Služby',
            'trzby_z_predaja_dm_a_mat' : None,
            'ostatne_vynosy_z_hc' : None,
            'vysledok_hosp_z_hc' : 'Výsledok hospodárenia z hospodárskej činnosti',
        },
    },
    694 : {
        'Strana aktív' : {
            'kratkodoby_fin_maj' : 'Finančný majetok',
            'zasoby' : 'Zásoby',
            'aktiva' : 'SPOLU MAJETOK',
            'dlhodobe_pohladavky' : 'Dlhodobé pohľadávky',
            'neobezny_majetok' : 'Neobežný majetok',
            'kratkodobe_pohladavky' : 'Krátkodobé pohľadávky',
            'financne_ucty' : None,
        },
        'Strana pasív' : {
            'kratkodobe_zavazky' : 'Krátkodobé záväzky okrem rezerv, úverov a výpomoci súčet',
            'kratkodobe_bank_uv' : 'Bežné bankové úvery',
            'kratkodobe_fin_vyp' : 'Krátkodobé finančné výpomoci',
            'vlastne_imanie' : 'Vlastné imanie',
            'zavazky' : 'Záväzky',
            'dlhodobe_zavazky' : 'Dlhodobé záväzky okrem rezerv a úverov',
            'dlhodobe_bank_uv' : 'Dlhodobé bankové úvery',
            'nerozdeleny_zisk' : 'Nerozdelený zisk alebo neuhradená strata minulých rokov',
        },
        'Výkaz ziskov a strát' : {
            'eat' : 'Výsledok hospodárenia za účtovné obdobie po zdanení',
            'odpisy' : 'Odpisy a opravné položky k dlhodobému nehmotnému majetku a dlhodobému hmotnému majetku',
            'pridana_hodnota' : 'Pridaná hodnota',
            'nahlady_na_hc' : 'Náklady na hospodársku činnosť spolu súčet',
            'vynosy_z_hc' : 'Výnosy z hospodárskej činnosti spolu súčet',
            'spotreba_mae' : 'Spotreba materiálu, energie a ostatných neskladovateľných dodávok',
            'ebt' : 'Výsledok hospodárenia za účtovné obdobie pred zdanením',
            'vynosy_z_fc' : 'Výnosy z finančnej činnosti spolu súčet',
            'trzby_z_predaja_cp' : None,
            'predane_cenne_papiere' : None,
            'vynosy_z_dlhod_fin_maj' : None,
            'vynosy_z_kratk_fin_maj' : None,
            'vynosy_z_precenenia_cp' : None,
            'vynosove_uroky' : None,
            'kurzove_zisky' : None,
            'ostatne_vynosy_z_fc' : None,
            'trzby_za_tovar' : 'Tržby z predaja tovaru',
            'trzby_za_vyrobky' : 'Tržby z predaja vlastných výrobkov a služieb',
            'trzby_za_sluzby' : None,
            'obchodna_marza' : None,
            'zmena_stavu_vo_zasob' : 'Zmena stavu vnútroorganizačných zásob',
            'aktivacia' : 'Aktivácia',
            'naklady_na_predany_tovar' : 'Náklady vynaložené na obstaranie predaného tovaru',
            'nakladove_uroky' : 'Nákladové úroky',
            'vyrobna_spotreba' : None,
            'vyroba' : None,
            'sluzby' : 'Služby',
            'trzby_z_predaja_dm_a_mat' : None,
            'ostatne_vynosy_z_hc' : None,
            'vysledok_hosp_z_hc' : 'Výsledok hospodárenia z hospodárskej činnosti',
        },
    },
    21 : {
        'Strana aktív' : {
            'kratkodoby_fin_maj' : None,
            'zasoby' : 'Zásoby súčet',
            'aktiva' : 'SPOLU MAJETOK',
            'dlhodobe_pohladavky' : 'Dlhodobé pohľadávky súčet',
            'neobezny_majetok' : 'Neobežný majetok',
            'kratkodobe_pohladavky' : 'Krátkodobé pohľadávky súčet',
            'financne_ucty' : 'Finančné účty súčet',
        },
        'Strana pasív' : {
            'kratkodobe_zavazky' : 'Krátkodobé záväzky súčet',
            'kratkodobe_bank_uv' : 'Bežné bankové úvery',
            'kratkodobe_fin_vyp' : 'Krátkodobé finančné výpomoci',
            'vlastne_imanie' : 'Vlastné imanie',
            'zavazky' : 'Záväzky',
            'dlhodobe_zavazky' : 'Dlhodobé záväzky súčet',
            'dlhodobe_bank_uv' : 'Bankové úvery dlhodobé',
            'nerozdeleny_zisk' : 'Výsledok hospodárenia minulých rokov',
        },
    },
    22 : {
        'Výkaz ziskov a strát' : {
            'eat' : 'Výsledok hospodárenia za účtovné obdobie po zdanení',
            'odpisy' : 'Odpisy a opravné položky k dlhodobému nehmotnému majetku a dlhodobému hmotnému majetku',
            'pridana_hodnota' : 'Pridaná hodnota',
            'nahlady_na_hc' : None,
            'vynosy_z_hc' : None,
            'spotreba_mae' : 'Spotreba materiálu, energie a ostatných neskladovateľných dodávok',
            'ebt' : 'Výsledok hospodárenia za účtovné obdobie pred zdanením',
            'vynosy_z_fc' : None,
            'trzby_z_predaja_cp' : 'Tržby z predaja cenných papierov a podielov',
            'predane_cenne_papiere' : 'Predané cenné papiere a podiely',
            'vynosy_z_dlhod_fin_maj' : 'Výnosy z dlhodobého finančného majetku',
            'vynosy_z_kratk_fin_maj' : 'Výnosy z krátkodobého finančného majetku',
            'vynosy_z_precenenia_cp' : 'Výnosy z precenenia cenných papierov a výnosy z derivátových operácií',
            'vynosove_uroky' : 'Výnosové úroky',
            'kurzove_zisky' : 'Kurzové zisky',
            'ostatne_vynosy_z_fc' : 'Ostatné výnosy z finančnej činnosti',
            'trzby_za_tovar' : 'Tržby z predaja tovaru',
            'trzby_za_vyrobky' : 'Tržby z predaja vlastných výrobkov a služieb',
            'trzby_za_sluzby' : None,
            'obchodna_marza' : 'Obchodná marža',
            'zmena_stavu_vo_zasob' : 'Zmeny stavu vnútroorganizačných zásob',
            'aktivacia' : 'Aktivácia',
            'naklady_na_predany_tovar' : 'Náklady vynaložené na obstaranie predaného tovaru',
            'nakladove_uroky' : 'Nákladové úroky',
            'vyrobna_spotreba' : 'Výrobná spotreba',
            'vyroba' : 'Výroba',
            'sluzby' : 'Služby',
            'trzby_z_predaja_dm_a_mat' : 'Tržby z predaja dlhodobého majetku a materiálu',
            'ostatne_vynosy_z_hc' : 'Ostatné výnosy z hospodárskej činnosti',
            'vysledok_hosp_z_hc' : 'Výsledok hospodárenia z hospodárskej činnosti',
        },
    },
}