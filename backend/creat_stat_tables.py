import psycopg2
import pandas as pd
import numpy as np
import firebase_admin
from firebase_admin import firestore, credentials

firebase_admin.initialize_app(credentials.Certificate('firebase_secret_cred.json'))
firestore_db = firestore.client()

def get_col_for_sql(data, command='') :
    for i in data : command += i + ', '
    return command[:len(command)-2]

def grouped_agr_func(ind_lst) :
    o = {}
    for i in ind_lst : o[i] = [np.mean, 'median', 'std']
    return o

psgconn = psycopg2.connect("dbname=indicators user=postgres password='admin'")

calc_feed = pd.read_sql("SELECT * FROM calc_feed", psgconn)
calc_output = pd.read_sql("SELECT * FROM calc_output", psgconn)

psgconn.close()

database = {
    'calc_feed' : {
        'data' : calc_feed, 
        'pattern' : list(calc_feed.columns)[7:]
    },
    'calc_output' : {
        'data' : calc_output, 
        'pattern' : list(calc_output.columns)[7:]
    }
}

search_by = ['odvetvie', 'sknace_code', 'okres']
grand_db = {}
for source in database :
    for index in search_by :
        table_name = index + '_' + source
        pivot = database[source]['data'].pivot_table(index=[index, 'year'], values=database[source]['pattern'], aggfunc=grouped_agr_func(database[source]['pattern'])).reset_index()
        pivot.columns = ['_'.join(i) for i in pivot.columns.to_flat_index()]
        grand_db[table_name] = {}
        pivot = pivot.to_dict('records')
        for row in pivot : 
            if row[index + '_'] not in grand_db[table_name] : grand_db[table_name][row[index + '_']] = {}
            for indicator in row : 
                if indicator not in ['year_', index + '_'] :
                    if indicator not in grand_db[table_name][row[index + '_']] : grand_db[table_name][row[index + '_']][indicator] = {}
                    grand_db[table_name][row[index + '_']][indicator][str(row['year_'])] = row[indicator]

for table_name in grand_db :
    for index in grand_db[table_name] :
        print(table_name, index)
        firestore_db.collection(table_name).document(index.replace('#', '')).set(grand_db[table_name][index])

