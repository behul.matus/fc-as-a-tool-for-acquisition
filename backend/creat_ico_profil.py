import psycopg2
import pandas as pd
import firebase_admin
from firebase_admin import firestore, credentials

firebase_admin.initialize_app(credentials.Certificate('firebase_secret_cred.json'))
firestore_db = firestore.client()

psgconn = psycopg2.connect("dbname=indicators user=postgres password='admin'")
psgcur = psgconn.cursor()

data_input = pd.read_sql("SELECT * FROM data_input", psgconn)
calc_output = pd.read_sql("SELECT * FROM calc_output", psgconn)
calc_feed = pd.read_sql("SELECT * FROM calc_feed", psgconn)

data_input = data_input.drop(columns=['data']).to_dict('records')
calc_output = calc_output.to_dict('records')
calc_feed = calc_feed.to_dict('records')

years = []
upload_calc = {}
for row in calc_output :
    ico = row['ico'].replace('#', '')
    if ico not in upload_calc : upload_calc[ico] = {}
    for indicator in row :
        if indicator not in ['year', 'okres', 'odvetvie', 'sknace_code', 'employee_size', 'ico', 'year', 'pravna_forma'] :
            if indicator not in upload_calc[ico] : upload_calc[ico][indicator] = {}
            upload_calc[ico][indicator][str(row['year'])] = row[indicator]
            if str(row['year']) not in years : years.append(str(row['year']))

upload_feed = {}
for row in calc_feed :
    ico = row['ico'].replace('#', '')
    if ico not in upload_feed : upload_feed[ico] = {}
    for indicator in row :
        if indicator not in ['year', 'okres', 'odvetvie', 'sknace_code', 'employee_size', 'ico', 'year', 'pravna_forma'] :
            if indicator not in upload_feed[ico] : upload_feed[ico][indicator] = {}
            upload_feed[ico][indicator][str(row['year'])] = row[indicator]

upload_data = {}
config_comp = {}
for row in data_input :
    ico = row['ico'].replace('#', '')
    upload_data[ico] = {
        'Názov' : row['nazov'],
        'sknace_name' : row['sknace_name'].replace('#', ''), 
        'Okres' : row['okres'], 
        'Odvetvie' : row['odvetvie'], 
        'SK NACE' : row['sknace_code'].replace('#', ''), 
        'Dátum vzniku' : row['creation_date'], 
        'Právna forma' : row['pravna_forma'], 
        'Počet zamestnancov' : row['employee_size'], 
        'calc_feed' : upload_feed[ico],
        'calc_output' : upload_calc[ico],
        'years' : years
    }
    # for year in upload_calc[row['ico']] :
    #     upload_data[ico]['data'][str(year)] = upload_calc[row['ico']][year]

    firestore_db.collection('comp_list').document(ico).set(upload_data[ico])

    config_comp[ico] = True

try : firestore_db.collection('config').document('companies').update(config_comp)
except : firestore_db.collection('config').document('companies').set(config_comp)





