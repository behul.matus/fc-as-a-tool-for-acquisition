import time, requests, json, traceback, psycopg2

from indicators import data_structure, indicators_structure
import indicators as ind

psgconn = psycopg2.connect("dbname=indicators user=postgres password='admin'")
psgcur = psgconn.cursor()


base_url = 'https://www.registeruz.sk/cruz-public/api'

uctovna_jednotka_id_url = base_url + '/uctovne-jednotky?zmenene-od=2000-01-01&ico={ICO}'
uctovna_jednotka_url = base_url + '/uctovna-jednotka?id={UCTJED}'
uctovna_zavierka_url = base_url + '/uctovna-zavierka?id={ZAVRID}'
uctovny_vykaz_url = base_url + '/uctovny-vykaz?id={VYKID}'
sablona_url = base_url + '/sablona?id={SABLONA}'
pravne_formy_url = base_url + '/pravne-formy'
velkosti_organizacie_url = base_url + '/velkosti-organizacie'
sk_nace_url = base_url + '/sk-nace'
okresy_url = base_url + '/okresy'

pravne_formy = {}
velkosti_organizacie = {}
sk_nace = {}
sablony_dict = {}
okresy = {}

data_input_command = """
    INSERT INTO data_input (
        ico, 
        okres, 
        kraj, 
        odvetvie, 
        sknace_code, 
        sknace_name, 
        creation_date, 
        pravna_forma, 
        employee_size, 
        nazov, 
        data
    ) VALUES (
        %(ico)s, 
        %(okres)s, 
        %(kraj)s, 
        %(odvetvie)s, 
        %(sknace_code)s, 
        %(sknace_name)s, 
        %(creation_date)s, 
        %(pravna_forma)s, 
        %(employee_size)s, 
        %(nazov)s, 
        %(data)s
    )
"""
calc_feed_command = """
    INSERT INTO calc_feed (
        ico, 
        year, 
        okres, 
        kraj, 
        odvetvie, 
        sknace_code, 
        pravna_forma, 
        employee_size, 
        kratkodoby_fin_maj, 
        zasoby, 
        aktiva, 
        dlhodobe_pohladavky, 
        neobezny_majetok, 
        kratkodobe_pohladavky, 
        financne_ucty, 
        kratkodobe_zavazky, 
        kratkodobe_bank_uv, 
        kratkodobe_fin_vyp, 
        vlastne_imanie, 
        zavazky, 
        dlhodobe_zavazky, 
        dlhodobe_bank_uv, 
        nerozdeleny_zisk, 
        eat, 
        odpisy, 
        pridana_hodnota, 
        nahlady_na_hc, 
        vynosy_z_hc, 
        spotreba_mae, 
        ebt, 
        vynosy_z_fc, 
        trzby_z_predaja_cp, 
        predane_cenne_papiere, 
        vynosy_z_dlhod_fin_maj, 
        vynosy_z_kratk_fin_maj, 
        vynosy_z_precenenia_cp, 
        vynosove_uroky, 
        kurzove_zisky, 
        ostatne_vynosy_z_fc, 
        trzby_za_tovar, 
        trzby_za_vyrobky, 
        trzby_za_sluzby, 
        obchodna_marza, 
        zmena_stavu_vo_zasob, 
        aktivacia, 
        naklady_na_predany_tovar, 
        nakladove_uroky, 
        vyrobna_spotreba, 
        vyroba, 
        sluzby, 
        trzby_z_predaja_dm_a_mat, 
        ostatne_vynosy_z_hc, 
        vysledok_hosp_z_hc, 
        trzby, 
        ebit, 
        cpk    
    ) VALUES (
        %(ico)s, 
        %(year)s, 
        %(okres)s, 
        %(kraj)s, 
        %(odvetvie)s, 
        %(sknace_code)s, 
        %(pravna_forma)s, 
        %(employee_size)s, 
        %(kratkodoby_fin_maj)s, 
        %(zasoby)s, 
        %(aktiva)s, 
        %(dlhodobe_pohladavky)s, 
        %(neobezny_majetok)s, 
        %(kratkodobe_pohladavky)s, 
        %(financne_ucty)s, 
        %(kratkodobe_zavazky)s, 
        %(kratkodobe_bank_uv)s, 
        %(kratkodobe_fin_vyp)s, 
        %(vlastne_imanie)s, 
        %(zavazky)s, 
        %(dlhodobe_zavazky)s, 
        %(dlhodobe_bank_uv)s, 
        %(nerozdeleny_zisk)s, 
        %(eat)s, 
        %(odpisy)s, 
        %(pridana_hodnota)s, 
        %(nahlady_na_hc)s, 
        %(vynosy_z_hc)s, 
        %(spotreba_mae)s, 
        %(ebt)s, 
        %(vynosy_z_fc)s, 
        %(trzby_z_predaja_cp)s, 
        %(predane_cenne_papiere)s, 
        %(vynosy_z_dlhod_fin_maj)s, 
        %(vynosy_z_kratk_fin_maj)s, 
        %(vynosy_z_precenenia_cp)s, 
        %(vynosove_uroky)s, 
        %(kurzove_zisky)s, 
        %(ostatne_vynosy_z_fc)s, 
        %(trzby_za_tovar)s, 
        %(trzby_za_vyrobky)s, 
        %(trzby_za_sluzby)s, 
        %(obchodna_marza)s, 
        %(zmena_stavu_vo_zasob)s, 
        %(aktivacia)s, 
        %(naklady_na_predany_tovar)s, 
        %(nakladove_uroky)s, 
        %(vyrobna_spotreba)s, 
        %(vyroba)s, 
        %(sluzby)s, 
        %(trzby_z_predaja_dm_a_mat)s, 
        %(ostatne_vynosy_z_hc)s, 
        %(vysledok_hosp_z_hc)s, 
        %(trzby)s, 
        %(ebit)s, 
        %(cpk)s  
    )
"""
calc_output_command = """
    INSERT INTO calc_output (
        ico,
        year,
        okres, 
        kraj, 
        odvetvie, 
        sknace_code, 
        pravna_forma, 
        employee_size,
        navratnost_cudzieho_kapitalu, 
        likvidita_1, 
        likvidita_2, 
        likvidita_3, 
        stupen_samofinancovania, 
        uverova_zadlzenost_aktiv, 
        tokove_zadlzenie, 
        rentabilita_aktiv, 
        rentabilita_vlastneho_kapitalu, 
        rentabilita_trzieb, 
        pridana_hodnota_na_trzbach, 
        doba_obratu_zasob, 
        doba_inkasa_pohladavok, 
        doba_splatnosti_zavazkov, 
        obrat_aktiv, 
        prevadzkova_nakladovost, 
        materialova_nakladovost, 
        cisty_pracovny_kapital_na_aktivach, 
        nerozdeleny_zisk_na_aktivach, 
        ebit_na_aktivach, 
        vlastne_imanie_na_zavazkoch, 
        trzby_na_aktivach, 
        ebit_na_kratkodobych_zavazkoch, 
        aktiva_na_zavazkoch, 
        ebit_na_trzbach, 
        zasoby_na_trzbach, 
        cash_flow_I, 
        cash_flow_II, 
        cash_flow_III, 
        cash_flow_na_vynosoch, 
        cash_flow_na_zavazkoch, 
        kralickov_rychly_test, 
        springate_model, 
        altmanovo_z_score, 
        index_bonity, 
        zlate_bilancne_pravidlo 
    ) VALUES (
        %(ico)s,
        %(year)s,
        %(okres)s, 
        %(kraj)s, 
        %(odvetvie)s, 
        %(sknace_code)s, 
        %(pravna_forma)s, 
        %(employee_size)s, 
        %(navratnost_cudzieho_kapitalu)s, 
        %(likvidita_1)s, 
        %(likvidita_2)s, 
        %(likvidita_3)s, 
        %(stupen_samofinancovania)s, 
        %(uverova_zadlzenost_aktiv)s, 
        %(tokove_zadlzenie)s, 
        %(rentabilita_aktiv)s, 
        %(rentabilita_vlastneho_kapitalu)s, 
        %(rentabilita_trzieb)s, 
        %(pridana_hodnota_na_trzbach)s, 
        %(doba_obratu_zasob)s, 
        %(doba_inkasa_pohladavok)s, 
        %(doba_splatnosti_zavazkov)s, 
        %(obrat_aktiv)s, 
        %(prevadzkova_nakladovost)s, 
        %(materialova_nakladovost)s, 
        %(cisty_pracovny_kapital_na_aktivach)s, 
        %(nerozdeleny_zisk_na_aktivach)s, 
        %(ebit_na_aktivach)s, 
        %(vlastne_imanie_na_zavazkoch)s, 
        %(trzby_na_aktivach)s, 
        %(ebit_na_kratkodobych_zavazkoch)s, 
        %(aktiva_na_zavazkoch)s, 
        %(ebit_na_trzbach)s, 
        %(zasoby_na_trzbach)s, 
        %(cash_flow_I)s, 
        %(cash_flow_II)s, 
        %(cash_flow_III)s, 
        %(cash_flow_na_vynosoch)s, 
        %(cash_flow_na_zavazkoch)s, 
        %(kralickov_rychly_test)s, 
        %(springate_model)s, 
        %(altmanovo_z_score)s, 
        %(index_bonity)s, 
        %(zlate_bilancne_pravidlo)s
    )
"""

for group in json.loads(requests.get(pravne_formy_url).content)['klasifikacie'] : pravne_formy[group['kod']] = group['nazov']['sk']
for group in json.loads(requests.get(velkosti_organizacie_url).content)['klasifikacie'] : velkosti_organizacie[group['kod']] = group['nazov']['sk'].split(' ')[0]
for group in json.loads(requests.get(sk_nace_url).content)['klasifikacie'] : sk_nace[group['kod']] = group['nazov']['sk']
for group in json.loads(requests.get(okresy_url).content)['lokacie'] : okresy[group['kod']] = group['nazov']['sk']

psgcur.execute('SELECT * FROM open_companies WHERE Okres = "Nitriansky"')
conf_comp_dict = psgcur.fetchall()
print('Total len of database is %s rows' % len(conf_comp_dict))
c = 1

for company in conf_comp_dict :
    ico = str(company[0])
    while len(ico) < 8 : ico = '0' + ico
    key_ico = '#' + ico
    company_profile = {'ico' : key_ico}

    print('Data from {} at'.format(ico), time.ctime(), ' - c.', c)
    c += 1
    
    indicators_inputs = {}
    database = {}
    dates_list = []

    url = uctovna_jednotka_id_url.format(ICO=ico)
    try : uctovna_jednotka_id = json.loads(requests.get(url).content)
    except : pass

    url = uctovna_jednotka_url.format(UCTJED=uctovna_jednotka_id['id'][0])
    try : uctovna_jednotka = json.loads(requests.get(url).content)
    except : pass

    if 'skNace' in uctovna_jednotka : 
        company_profile['sknace_name'] = '#' + sk_nace[uctovna_jednotka['skNace']]
        company_profile['sknace_code'] = '#' + uctovna_jednotka['skNace']
    else :
        company_profile['sknace_name'] = company[7].split('[')[0].strip()
        company_profile['sknace_code'] = company[7].split('[')[1].replace(']', '').strip()

    if 'datumZalozenia' in uctovna_jednotka : company_profile['creation_date'] = uctovna_jednotka['datumZalozenia']
    else : company_profile['creation_date'] = None

    if 'pravnaForma' in uctovna_jednotka : company_profile['pravna_forma'] = pravne_formy[uctovna_jednotka['pravnaForma']]
    else : company_profile['pravna_forma'] = None
    
    if 'velkostOrganizacie' in uctovna_jednotka : company_profile['employee_size'] = velkosti_organizacie[uctovna_jednotka['velkostOrganizacie']]
    else : company_profile['employee_size'] = company[5]
    
    if 'okres' in uctovna_jednotka : company_profile['okres'] = okresy[uctovna_jednotka['okres']]
    else : company_profile['okres'] = company[3]

    company_profile['kraj'] = company[4]
    
    if 'nazovUJ' in uctovna_jednotka : company_profile['nazov'] = uctovna_jednotka['nazovUJ']
    else : company_profile['nazov'] = company[1]
    
    company_profile['odvetvie'] = company[6]

    for uzid in uctovna_jednotka['idUctovnychZavierok'] :
        url = uctovna_zavierka_url.format(ZAVRID=uzid)
        try : uctovna_zavierka = json.loads(requests.get(url).content)
        except : pass

        for uvid in uctovna_zavierka['idUctovnychVykazov'] :
            url = uctovny_vykaz_url.format(VYKID=uvid)
            try : uctovny_vykaz = json.loads(requests.get(url).content)
            except : pass

            sablona_id = uctovny_vykaz['idSablony']
            if sablona_id not in data_structure : continue
            try : date_var = uctovny_vykaz['obsah']['titulnaStrana']['obdobieDo']
            except : 
                # print(ico, sablona_id, uvid, 'date_var creation')
                continue

            if sablona_id in sablony_dict : 
                sablona = sablony_dict[sablona_id]  
            else :
                url = sablona_url.format(SABLONA=sablona_id)
                try : 
                    sablona = json.loads(requests.get(url).content)
                    sablony_dict[sablona_id] = sablona
                except : pass

            if 'tabulky' not in sablona : 
                # print(ico, date_var, sablona_id, 'tabulky not in sablona')
                continue

            if date_var not in dates_list : dates_list.append(date_var)
            data = {}
            select_right_col = {}

            for data_set in sablona['tabulky'] :
                data_set_name = data_set['nazov']['sk']
                if data_set_name not in database : database[data_set_name] = {}
                if date_var not in database[data_set_name] : database[data_set_name][date_var] = {}

                select_right_col[data_set_name] = data_set['pocetDatovychStlpcov'] - 2

                data[data_set_name] = {
                    'names' : [],
                    'data' : [],
                }

                for row in data_set['riadky'] :
                    data[data_set_name]['names'].append(row['text']['sk'].split('(')[0].split('r.')[0].split('+')[0].strip())

            table_count = 0
            for data_set in uctovny_vykaz['obsah']['tabulky'] :
                data_set_name = data_set['nazov']['sk']
                work_list = []
                counter = 0

                for data_val in data_set['data'] :
                    if counter == select_right_col[data_set_name] : 
                        if data_val == '' : data_val = 0

                        data[data_set_name]['data'].append(int(data_val))

                    counter += 1
                    if counter == sablona['tabulky'][table_count]['pocetDatovychStlpcov'] : counter = 0

                table_count += 1
                for i in range(len(data[data_set_name]['data'])) :
                    database[data_set_name][date_var][data[data_set_name]['names'][i]] = data[data_set_name]['data'][i]
    


                
                for key in data_structure[sablona_id][data_set_name] :
                    if date_var not in indicators_inputs : 
                        indicators_inputs[date_var] = {
                            'ico' : key_ico, 
                            'year' : int(date_var.split('-')[0]),
                            'okres' : company_profile['okres'],
                            'odvetvie' : company_profile['odvetvie'],
                            'sknace_code' : company_profile['sknace_code'],
                            'pravna_forma' : company_profile['pravna_forma'],
                            'employee_size' : company_profile['employee_size']
                        }
                    
                    if data_structure[sablona_id][data_set_name][key] is None : 
                        indicators_inputs[date_var][key] = 0

                    else :
                        indicators_inputs[date_var][key] = database[data_set_name][date_var][data_structure[sablona_id][data_set_name][key]]

    company_profile['data'] = str(database)
    try :
        psgcur.execute(data_input_command, company_profile)
    except psycopg2.errors.UniqueViolation :
        print(('Key error at company_profile ', company_profile['ico']))
        print(traceback.format_exc())
        continue
    except :
        print(traceback.format_exc())

    dates_list.sort()

    for date_var in dates_list :
        indicators_output = indicators_structure.copy()
        indicators_output['ico'] = key_ico
        indicators_output['year'] = int(date_var.split('-')[0])
        indicators_output['okres'] = company_profile['okres']
        indicators_output['kraj'] = company_profile['kraj']
        indicators_output['odvetvie'] = company_profile['odvetvie']
        indicators_output['sknace_code'] = company_profile['sknace_code']
        indicators_output['pravna_forma'] = company_profile['pravna_forma']
        indicators_output['employee_size'] = company_profile['employee_size']

        date_before_var = str(int(date_var.split('-')[0]) - 1) + '-' + date_var.split('-')[1]

        if 'trzby' not in indicators_inputs : indicators_inputs[date_var]['trzby'] = {}
        if 'ebit' not in indicators_inputs : indicators_inputs[date_var]['ebit'] = {}
        if 'cpk' not in indicators_inputs : indicators_inputs[date_var]['cpk'] = {}

        try : 
            indicators_inputs[date_var]['trzby'] = indicators_inputs[date_var]['trzby_za_tovar'] + indicators_inputs[date_var]['trzby_za_vyrobky'] + indicators_inputs[date_var]['trzby_za_sluzby']
        except : indicators_inputs[date_var]['trzby'] = 0

        try : 
            indicators_inputs[date_var]['ebit'] = indicators_inputs[date_var]['ebt'] + indicators_inputs[date_var]['nakladove_uroky']
        except : 
            indicators_inputs[date_var]['ebit'] = indicators_inputs[date_var]['ebt']
        
        try :
            if indicators_inputs[date_var]['vyrobna_spotreba'] == 0 : 
                indicators_inputs[date_var]['vyrobna_spotreba'] = indicators_inputs[date_var]['spotreba_mae'] + indicators_inputs[date_var]['sluzby']
        except : pass

        try :
            if indicators_inputs[date_var]['vyroba'] == 0 : 
                indicators_inputs[date_var]['vyroba'] = indicators_inputs[date_var]['zmena_stavu_vo_zasob'] + indicators_inputs[date_var]['aktivacia']
        except : pass

        try :
            if indicators_inputs[date_var]['obchodna_marza'] == 0 : 
                indicators_inputs[date_var]['obchodna_marza'] = indicators_inputs[date_var]['trzby'] - indicators_inputs[date_var]['naklady_na_predany_tovar']
        except : pass

        try :
            if indicators_inputs[date_var]['pridana_hodnota'] == 0 : 
                indicators_inputs[date_var]['pridana_hodnota'] = indicators_inputs[date_var]['obchodna_marza'] + indicators_inputs[date_var]['vyroba'] - indicators_inputs[date_var]['vyrobna_spotreba']
        except : pass

        try :
            indicators_inputs[date_var]['kratkodoby_fin_maj'] = indicators_inputs[date_var]['kratkodoby_fin_maj'] + indicators_inputs[date_var]['financne_ucty']
        except : pass

        try :
            if indicators_inputs[date_var]['vynosy_z_fc'] == 0 : 
                indicators_inputs[date_var]['vynosy_z_fc'] = indicators_inputs[date_var]['trzby_z_predaja_cp'] + indicators_inputs[date_var]['predane_cenne_papiere'] + indicators_inputs[date_var]['vynosy_z_dlhod_fin_maj'] + indicators_inputs[date_var]['vynosy_z_kratk_fin_maj'] + indicators_inputs[date_var]['vynosy_z_precenenia_cp'] + indicators_inputs[date_var]['vynosove_uroky'] + indicators_inputs[date_var]['kurzove_zisky'] + indicators_inputs[date_var]['ostatne_vynosy_z_fc']
        except : pass

        try :
            if indicators_inputs[date_var]['vynosy_z_hc'] == 0 : 
                indicators_inputs[date_var]['vynosy_z_hc'] = indicators_inputs[date_var]['trzby'] + indicators_inputs[date_var]['vyroba'] + indicators_inputs[date_var]['trzby_z_predaja_dm_a_mat'] + indicators_inputs[date_var]['ostatne_vynosy_z_hc']
        except : pass

        try :
            if indicators_inputs[date_var]['nahlady_na_hc'] == 0 : 
                indicators_inputs[date_var]['nahlady_na_hc'] = indicators_inputs[date_var]['vynosy_z_hc'] - indicators_inputs[date_var]['vysledok_hosp_z_hc']
        except : pass

        try :
            indicators_inputs[date_var]['cpk'] = indicators_inputs[date_var]['dlhodobe_zavazky'] + indicators_inputs[date_var]['vlastne_imanie'] - indicators_inputs[date_var]['nerozdeleny_zisk']
        except : 
            indicators_inputs[date_var]['cpk'] = 0

        try :
            psgcur.execute(calc_feed_command, indicators_inputs[date_var])
        except psycopg2.errors.UniqueViolation :
            print('Key error at indicators_inputs ', (indicators_inputs[date_var]['ico'], indicators_inputs[date_var]['year']))
            print(dates_list)
            print(traceback.format_exc())
            continue
        except :
            print(traceback.format_exc())

        try : indicators_output['likvidita_1'] = ind.likvidita_1(indicators_inputs[date_var]['kratkodoby_fin_maj'], indicators_inputs[date_var]['kratkodobe_zavazky'], indicators_inputs[date_var]['kratkodobe_bank_uv'], indicators_inputs[date_var]['kratkodobe_fin_vyp'])
        except : indicators_output['likvidita_1'] = 0

        try : indicators_output['likvidita_2'] = ind.likvidita_2(indicators_inputs[date_var]['kratkodobe_pohladavky'], indicators_inputs[date_var]['kratkodoby_fin_maj'], indicators_inputs[date_var]['kratkodobe_zavazky'], indicators_inputs[date_var]['kratkodobe_bank_uv'], indicators_inputs[date_var]['kratkodobe_fin_vyp'])
        except : indicators_output['likvidita_2'] = 0

        try : indicators_output['likvidita_3'] = ind.likvidita_3(indicators_inputs[date_var]['zasoby'], indicators_inputs[date_var]['kratkodobe_pohladavky'], indicators_inputs[date_var]['kratkodoby_fin_maj'], indicators_inputs[date_var]['kratkodobe_zavazky'], indicators_inputs[date_var]['kratkodobe_bank_uv'], indicators_inputs[date_var]['kratkodobe_fin_vyp'])
        except : indicators_output['likvidita_3'] = 0

        try : indicators_output['stupen_samofinancovania'] = ind.stupen_samofinancovania(indicators_inputs[date_var]['vlastne_imanie'], indicators_inputs[date_var]['aktiva'])
        except : indicators_output['stupen_samofinancovania'] = 0

        try : indicators_output['uverova_zadlzenost_aktiv'] = ind.uverova_zadlzenost_aktiv(indicators_inputs[date_var]['kratkodobe_bank_uv'] + indicators_inputs[date_var]['dlhodobe_bank_uv'], indicators_inputs[date_var]['aktiva'])
        except : indicators_output['uverova_zadlzenost_aktiv'] = 0

        try : indicators_output['rentabilita_aktiv'] = ind.rentabilita_aktiv(indicators_inputs[date_var]['eat'], indicators_inputs[date_var]['aktiva'])
        except : indicators_output['rentabilita_aktiv'] = 0

        try : indicators_output['rentabilita_vlastneho_kapitalu'] = ind.rentabilita_vlastneho_kapitalu(indicators_inputs[date_var]['eat'], indicators_inputs[date_var]['vlastne_imanie'])
        except : indicators_output['rentabilita_vlastneho_kapitalu'] = 0

        try : indicators_output['rentabilita_trzieb'] = ind.rentabilita_trzieb(indicators_inputs[date_var]['eat'], indicators_inputs[date_var]['trzby'])
        except : indicators_output['rentabilita_trzieb'] = 0

        try : indicators_output['pridana_hodnota_na_trzbach'] = ind.pridana_hodnota_na_trzbach(indicators_inputs[date_var]['pridana_hodnota'], indicators_inputs[date_var]['trzby'])
        except : indicators_output['pridana_hodnota_na_trzbach'] = 0

        try : indicators_output['doba_obratu_zasob'] = ind.doba_obratu_zasob(indicators_inputs[date_var]['zasoby'], indicators_inputs[date_var]['vynosy_z_hc'], 365)
        except : indicators_output['doba_obratu_zasob'] = 0

        try : indicators_output['doba_inkasa_pohladavok'] = ind.doba_inkasa_pohladavok(indicators_inputs[date_var]['dlhodobe_pohladavky'] + indicators_inputs[date_var]['kratkodobe_pohladavky'], indicators_inputs[date_var]['vynosy_z_hc'], 365)
        except : indicators_output['doba_inkasa_pohladavok'] = 0

        try : indicators_output['doba_splatnosti_zavazkov'] = ind.doba_splatnosti_zavazkov(indicators_inputs[date_var]['zavazky'], indicators_inputs[date_var]['vynosy_z_hc'], 365)
        except : indicators_output['doba_splatnosti_zavazkov'] = 0

        try : indicators_output['obrat_aktiv'] = ind.obrat_aktiv(indicators_inputs[date_var]['trzby'], indicators_inputs[date_var]['aktiva'])
        except : indicators_output['obrat_aktiv'] = 0

        try : indicators_output['prevadzkova_nakladovost'] = ind.prevadzkova_nakladovost(indicators_inputs[date_var]['nahlady_na_hc'], indicators_inputs[date_var]['vynosy_z_hc'])
        except : indicators_output['prevadzkova_nakladovost'] = 0

        try : indicators_output['materialova_nakladovost'] = ind.materialova_nakladovost(indicators_inputs[date_var]['spotreba_mae'], indicators_inputs[date_var]['vynosy_z_hc'])
        except : indicators_output['materialova_nakladovost'] = 0

        try : indicators_output['cisty_pracovny_kapital_na_aktivach'] = ind.cisty_pracovny_kapital_na_aktivach(indicators_inputs[date_var]['cpk'], indicators_inputs[date_var]['aktiva'])
        except : indicators_output['cisty_pracovny_kapital_na_aktivach'] = 0  

        try : indicators_output['nerozdeleny_zisk_na_aktivach'] = ind.nerozdeleny_zisk_na_aktivach(indicators_inputs[date_var]['nerozdeleny_zisk'], indicators_inputs[date_var]['aktiva'])
        except : indicators_output['nerozdeleny_zisk_na_aktivach'] = 0

        try : indicators_output['ebit_na_aktivach'] = ind.ebit_na_aktivach(indicators_inputs[date_var]['ebit'], indicators_inputs[date_var]['aktiva'])
        except : indicators_output['ebit_na_aktivach'] = 0

        try : indicators_output['vlastne_imanie_na_zavazkoch'] = ind.vlastne_imanie_na_zavazkoch(indicators_inputs[date_var]['vlastne_imanie'], indicators_inputs[date_var]['zavazky'])
        except : indicators_output['vlastne_imanie_na_zavazkoch'] = 0

        try : indicators_output['trzby_na_aktivach'] = ind.trzby_na_aktivach(indicators_inputs[date_var]['trzby'], indicators_inputs[date_var]['aktiva'])
        except : indicators_output['trzby_na_aktivach'] = 0

        try : indicators_output['ebit_na_kratkodobych_zavazkoch'] = ind.ebit_na_kratkodobych_zavazkoch(indicators_inputs[date_var]['ebit'], indicators_inputs[date_var]['kratkodobe_zavazky'])
        except : indicators_output['ebit_na_kratkodobych_zavazkoch'] = 0

        try : indicators_output['aktiva_na_zavazkoch'] = ind.aktiva_na_zavazkoch(indicators_inputs[date_var]['aktiva'], indicators_inputs[date_var]['zavazky'])
        except : indicators_output['aktiva_na_zavazkoch'] = 0

        try : indicators_output['ebit_na_trzbach'] = ind.ebit_na_trzbach(indicators_inputs[date_var]['ebit'], indicators_inputs[date_var]['trzby'])
        except : indicators_output['ebit_na_trzbach'] = 0

        try : indicators_output['zasoby_na_trzbach'] = ind.zasoby_na_trzbach(indicators_inputs[date_var]['zasoby'], indicators_inputs[date_var]['trzby'])
        except : indicators_output['zasoby_na_trzbach'] = 0

        try : indicators_output['cash_flow_I'] = ind.cash_flow_I(indicators_inputs[date_var]['eat'], indicators_inputs[date_var]['odpisy'])
        except : indicators_output['cash_flow_I'] = 0

        try : indicators_output['cash_flow_II'] = ind.cash_flow_II(indicators_output['cash_flow_I'], indicators_inputs[date_var]['zasoby'], indicators_inputs[date_before_var]['zasoby'])
        except : indicators_output['cash_flow_II'] = 0

        try : indicators_output['cash_flow_III'] = ind.cash_flow_III(indicators_output['cash_flow_II'], indicators_inputs[date_var]['kratkodobe_pohladavky'] + indicators_inputs[date_var]['dlhodobe_pohladavky'], indicators_inputs[date_before_var]['kratkodobe_pohladavky'] + indicators_inputs[date_var]['dlhodobe_pohladavky'], indicators_inputs[date_var]['kratkodobe_zavazky'], indicators_inputs[date_before_var]['kratkodobe_zavazky'])
        except : indicators_output['cash_flow_III'] = 0

        try : indicators_output['cash_flow_na_vynosoch'] = ind.cash_flow_na_vynosoch(indicators_output['cash_flow_I'], indicators_inputs[date_var]['vynosy_z_hc'], indicators_inputs[date_var]['vynosy_z_fc'])
        except : indicators_output['cash_flow_na_vynosoch'] = 0

        try : indicators_output['cash_flow_na_zavazkoch'] = ind.cash_flow_na_zavazkoch(indicators_output['cash_flow_I'], indicators_inputs[date_var]['zavazky'])
        except : indicators_output['cash_flow_na_zavazkoch'] = 0

        try : indicators_output['tokove_zadlzenie'] = ind.tokove_zadlzenie(indicators_inputs[date_var]['zavazky'], indicators_output['cash_flow_I'])
        except : indicators_output['tokove_zadlzenie'] = 0

        try : indicators_output['navratnost_cudzieho_kapitalu'] = ind.navratnost_cudzieho_kapitalu(indicators_inputs[date_var]['zavazky'], indicators_inputs[date_var]['kratkodoby_fin_maj'], indicators_output['cash_flow_I'])
        except : indicators_output['navratnost_cudzieho_kapitalu'] = 0

        try : indicators_output['kralickov_rychly_test'] = str(ind.kralickov_rychly_test(indicators_output['stupen_samofinancovania'], indicators_output['navratnost_cudzieho_kapitalu'], indicators_output['cash_flow_na_vynosoch'], indicators_output['rentabilita_aktiv']))
        except : indicators_output['kralickov_rychly_test'] = 0

        try : indicators_output['springate_model'] = ind.springate_model(indicators_output['cisty_pracovny_kapital_na_aktivach'], indicators_output['ebit_na_aktivach'], indicators_output['ebit_na_kratkodobych_zavazkoch'], indicators_output['trzby_na_aktivach'])
        except : indicators_output['springate_model'] = 0

        try : indicators_output['altmanovo_z_score'] = ind.altmanovo_z_score(indicators_output['cisty_pracovny_kapital_na_aktivach'], indicators_output['nerozdeleny_zisk_na_aktivach'], indicators_output['ebit_na_aktivach'], indicators_output['vlastne_imanie_na_zavazkoch'], indicators_output['trzby_na_aktivach'])
        except : indicators_output['altmanovo_z_score'] = 0

        try : indicators_output['index_bonity'] = ind.index_bonity(indicators_output['cash_flow_na_zavazkoch'], indicators_output['aktiva_na_zavazkoch'], indicators_output['ebit_na_aktivach'], indicators_output['ebit_na_trzbach'], indicators_output['zasoby_na_trzbach'], indicators_output['trzby_na_aktivach'])
        except : indicators_output['index_bonity'] = 0

        try : indicators_output['zlate_bilancne_pravidlo'] = ind.zlate_bilancne_pravidlo(indicators_inputs[date_var]['dlhodobe_zavazky'], indicators_inputs[date_var]['nerozdeleny_zisk'], indicators_inputs[date_var]['vlastne_imanie'])
        except : indicators_output['zlate_bilancne_pravidlo'] = 0

        try :
            psgcur.execute(calc_output_command, indicators_output)
        except psycopg2.errors.UniqueViolation :
            print('Key error at indicators_output ', (indicators_output['ico'], indicators_output['year']))
            print(dates_list)
            print(traceback.format_exc())
            continue
        except :
            print(traceback.format_exc())

    psgconn.commit()

