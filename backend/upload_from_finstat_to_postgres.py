import pandas as pd
from sqlalchemy import create_engine

engine = create_engine("postgresql://postgres:admin@localhost:5432/indicators")

database = pd.read_excel('export_closed.xlsx')[['IČO', 'Názov', 'Mesto', 'Okres', 'Kraj', 'Kategória zamestnancov', 'Odvetvie', 'SK NACE', 'Druh vlastníctva']]
database.to_sql(name='closed_companies', con=engine, if_exists='replace', index=False)

database = pd.read_excel('export_conso.xlsx')[['IČO', 'Názov', 'Mesto', 'Okres', 'Kraj', 'Kategória zamestnancov', 'Odvetvie', 'SK NACE', 'Druh vlastníctva']]
database.to_sql(name='open_companies', con=engine, if_exists='replace', index=False)





