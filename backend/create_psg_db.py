import psycopg2, traceback

psgconn = psycopg2.connect("dbname=indicators user=postgres password='admin'")
psgcur = psgconn.cursor()

commands = {
    'data_input' : """
        CREATE TABLE data_input (
            ico VARCHAR(255) PRIMARY KEY, 
            okres VARCHAR(255) NOT NULL, 
            kraj VARCHAR(255) NOT NULL, 
            odvetvie VARCHAR(255) NOT NULL,
            sknace_code VARCHAR(255) NOT NULL, 
            sknace_name VARCHAR(255), 
            creation_date VARCHAR(255), 
            pravna_forma VARCHAR(255), 
            employee_size VARCHAR(255) NOT NULL, 
            nazov VARCHAR(255) NOT NULL,
            data TEXT NOT NULL
        )
    """,
    'calc_feed' : """
        CREATE TABLE calc_feed (
            ico VARCHAR(255) NOT NULL,
            year INTEGER NOT NULL,
            okres VARCHAR(255) NOT NULL, 
            kraj VARCHAR(255) NOT NULL, 
            odvetvie VARCHAR(255) NOT NULL,
            sknace_code VARCHAR(255) NOT NULL, 
            pravna_forma VARCHAR(255), 
            employee_size VARCHAR(255) NOT NULL, 
            kratkodoby_fin_maj NUMERIC NOT NULL,
            zasoby NUMERIC NOT NULL,
            aktiva NUMERIC NOT NULL,
            dlhodobe_pohladavky NUMERIC NOT NULL,
            neobezny_majetok NUMERIC NOT NULL,
            kratkodobe_pohladavky NUMERIC NOT NULL,
            financne_ucty NUMERIC NOT NULL,
            kratkodobe_zavazky NUMERIC NOT NULL,
            kratkodobe_bank_uv NUMERIC NOT NULL,
            kratkodobe_fin_vyp NUMERIC NOT NULL,
            vlastne_imanie NUMERIC NOT NULL,
            zavazky NUMERIC NOT NULL,
            dlhodobe_zavazky NUMERIC NOT NULL,
            dlhodobe_bank_uv NUMERIC NOT NULL,
            nerozdeleny_zisk NUMERIC NOT NULL,
            eat NUMERIC NOT NULL,
            odpisy NUMERIC NOT NULL,
            pridana_hodnota NUMERIC NOT NULL,
            nahlady_na_hc NUMERIC NOT NULL,
            vynosy_z_hc NUMERIC NOT NULL,
            spotreba_mae NUMERIC NOT NULL,
            ebt NUMERIC NOT NULL,
            vynosy_z_fc NUMERIC NOT NULL,
            trzby_z_predaja_cp NUMERIC NOT NULL,
            predane_cenne_papiere NUMERIC NOT NULL,
            vynosy_z_dlhod_fin_maj NUMERIC NOT NULL,
            vynosy_z_kratk_fin_maj NUMERIC NOT NULL,
            vynosy_z_precenenia_cp NUMERIC NOT NULL,
            vynosove_uroky NUMERIC NOT NULL,
            kurzove_zisky NUMERIC NOT NULL,
            ostatne_vynosy_z_fc NUMERIC NOT NULL,
            trzby_za_tovar NUMERIC NOT NULL,
            trzby_za_vyrobky NUMERIC NOT NULL,
            trzby_za_sluzby NUMERIC NOT NULL,
            obchodna_marza NUMERIC NOT NULL,
            zmena_stavu_vo_zasob NUMERIC NOT NULL,
            aktivacia NUMERIC NOT NULL,
            naklady_na_predany_tovar NUMERIC NOT NULL,
            nakladove_uroky NUMERIC NOT NULL,
            vyrobna_spotreba NUMERIC NOT NULL,
            vyroba NUMERIC NOT NULL,
            sluzby NUMERIC NOT NULL,
            trzby_z_predaja_dm_a_mat NUMERIC NOT NULL,
            ostatne_vynosy_z_hc NUMERIC NOT NULL,
            vysledok_hosp_z_hc NUMERIC NOT NULL,
            trzby NUMERIC NOT NULL,
            ebit NUMERIC NOT NULL,
            cpk NUMERIC NOT NULL,
            PRIMARY KEY (ico , year)
        )
    """,
    'calc_output' : """
        CREATE TABLE calc_output (
            ico VARCHAR(255) NOT NULL,
            year INTEGER NOT NULL,
            okres VARCHAR(255) NOT NULL, 
            kraj VARCHAR(255) NOT NULL, 
            odvetvie VARCHAR(255) NOT NULL,
            sknace_code VARCHAR(255) NOT NULL, 
            pravna_forma VARCHAR(255), 
            employee_size VARCHAR(255) NOT NULL, 
            navratnost_cudzieho_kapitalu NUMERIC NOT NULL, 
            likvidita_1 NUMERIC NOT NULL, 
            likvidita_2 NUMERIC NOT NULL, 
            likvidita_3 NUMERIC NOT NULL, 
            stupen_samofinancovania NUMERIC NOT NULL, 
            uverova_zadlzenost_aktiv NUMERIC NOT NULL, 
            tokove_zadlzenie NUMERIC NOT NULL, 
            rentabilita_aktiv NUMERIC NOT NULL, 
            rentabilita_vlastneho_kapitalu NUMERIC NOT NULL, 
            rentabilita_trzieb NUMERIC NOT NULL, 
            pridana_hodnota_na_trzbach NUMERIC NOT NULL, 
            doba_obratu_zasob NUMERIC NOT NULL, 
            doba_inkasa_pohladavok NUMERIC NOT NULL, 
            doba_splatnosti_zavazkov NUMERIC NOT NULL, 
            obrat_aktiv NUMERIC NOT NULL, 
            prevadzkova_nakladovost NUMERIC NOT NULL, 
            materialova_nakladovost NUMERIC NOT NULL, 
            cisty_pracovny_kapital_na_aktivach NUMERIC NOT NULL, 
            nerozdeleny_zisk_na_aktivach NUMERIC NOT NULL, 
            ebit_na_aktivach NUMERIC NOT NULL, 
            vlastne_imanie_na_zavazkoch NUMERIC NOT NULL, 
            trzby_na_aktivach NUMERIC NOT NULL, 
            ebit_na_kratkodobych_zavazkoch NUMERIC NOT NULL, 
            aktiva_na_zavazkoch NUMERIC NOT NULL, 
            ebit_na_trzbach NUMERIC NOT NULL, 
            zasoby_na_trzbach NUMERIC NOT NULL, 
            cash_flow_I NUMERIC NOT NULL, 
            cash_flow_II NUMERIC NOT NULL, 
            cash_flow_III NUMERIC NOT NULL, 
            cash_flow_na_vynosoch NUMERIC NOT NULL, 
            cash_flow_na_zavazkoch NUMERIC NOT NULL, 
            kralickov_rychly_test NUMERIC NOT NULL, 
            springate_model NUMERIC NOT NULL, 
            altmanovo_z_score NUMERIC NOT NULL, 
            index_bonity NUMERIC NOT NULL, 
            zlate_bilancne_pravidlo NUMERIC NOT NULL, 
            PRIMARY KEY (ico , year)
        )
    """
}

# psgcur.execute('DELETE FROM data_input')
# psgconn.commit()


# psgcur.execute("Select * FROM data_input")
# headers = psgcur.description
# outcome = list(psgcur.fetchall())
# database = {}
# counter = 0
# for row in outcome :
#     for i in headers :
#         database[i[0]] = row[counter]
#         counter += 1
#     database['data'] = json.loads(database['data'].replace("'", '"'))
# print(database)

psgcur.execute("""
    DROP TABLE 
        data_input,
        calc_feed, 
        calc_output 
    CASCADE
""")
psgconn.commit()

for table in commands :
    try :
        psgcur.execute(commands[table])
        psgconn.commit()
        # psgcur.execute('DELETE FROM %s CASCADE' % table)
        # psgconn.commit()
    except :
        print(traceback.format_exc())

psgconn.close()