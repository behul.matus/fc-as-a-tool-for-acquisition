async function createCharts(evaluateBy) {
    // document.getElementById('putDataStatus').style.display = 'none';
    document.getElementById('searchingStatus').style.display = 'grid';

    if (searchByDict[evaluateBy] == 'okres') {
        searchBy = document.getElementById('companyOkres').innerHTML;
    } else if (searchByDict[evaluateBy] == 'odvetvie') {
        searchBy = document.getElementById('companyOdvetvie').innerHTML;
    } else if (searchByDict[evaluateBy] == 'sknace_code') {
        searchBy = document.getElementById('companySKNACE').innerHTML;
    };

    db.collection('comp_list').doc(icoInput.value).get().then((fb_company) => {
        fb_company = fb_company.data();
        db.collection(searchByDict[evaluateBy] + '_calc_output').doc(searchBy).get().then((fb_feed) => {
            fb_feed = sortObjectByKeys(fb_feed.data());

            document.getElementById('chartcanvas').innerHTML = ""
            let seriesData = []
            let labelsData = []

            for (indicator in sortObjectByKeys(fb_company['calc_output'])) {
                if (indicator in translateIndicator) {
                    titleTbl = document.createElement('table')
                    titleTbl.setAttribute('id', 'titleTable')
                    document.getElementById('chartcanvas').appendChild(titleTbl);
                    titleTbb = document.createElement('tbody')
                    titleTbl.appendChild(titleTbb)
                    titleTr = document.createElement('tr')
                    titleTbb.appendChild(titleTr)
                    titleTd = document.createElement('td')
                    titleTd.innerHTML = translateIndicator[indicator][0]
                    titleTr.appendChild(titleTd)

                    year = document.getElementById('yearId').value
                    std = Math.round(10000 * Number(fb_feed[indicator + '_std'][year])) / 10000
                    mean = Math.round(10000 * Number(fb_feed[indicator + '_mean'][year])) / 10000
                    comp = Math.round(10000 * Number(fb_company['calc_output'][indicator][year])) / 10000
                    topVar = mean + std
                    botVar = mean - std
                    // if (botVar < 0) {
                    //     editTop = topVar + botVar
                    //     editComp = comp + botVar
                    // } else {
                    //     editTop = topVar - botVar
                    //     editComp = comp - botVar
                    // }
                    editTop = topVar - botVar
                    editComp = comp - botVar

                    if (translateIndicator[indicator][1] == 1) {
                        ratio = editComp / editTop
                    } else if (translateIndicator[indicator][1] == -1) {
                        ratio = editTop / editComp - 1
                    }
                    labelsData.push(translateIndicator[indicator][0])
                    seriesData.push(ratio * 100)

                    // console.log(translateIndicator[indicator][0])
                    // console.log(editTop)
                    // console.log(botVar)
                    // console.log(std)
                    // console.log(comp)
                    // console.log(editComp)
                    // console.log('-----------')

                    titleTd = document.createElement('td')
                    titleTd.setAttribute('name', gradeRatio(ratio))
                    titleTd.innerHTML = gradeRatio(ratio)
                    titleTr.appendChild(titleTd)
                    
                    formRow = document.createElement('div');
                    formRow.setAttribute('class', 'row-chart');
                    document.getElementById('chartcanvas').appendChild(formRow);

                    chartRow = document.createElement('div');
                    chartRow.setAttribute('class', 'row-element');
                    formRow.appendChild(chartRow)                       

                    altmanInd = document.createElement('div');
                    altmanInd.setAttribute('id', indicator + 'Ind');
                    altmanInd.setAttribute('class', 'indic-chart');
                    chartRow.appendChild(altmanInd);
                    chart = new ApexCharts(altmanInd, getOptionLineChart(
                                                                Object.values(fb_company['calc_output'][indicator]), 
                                                                Object.values(fb_feed[indicator + '_mean']), 
                                                                Object.values(fb_feed[indicator + '_median']), 
                                                                Object.values(fb_company['years']), evaluateBy
                    ));
                    chart.render();

                    med_corr = [];
                    mean_corr = [];

                    for (year in fb_company['calc_output'][indicator]) {
                        med_corr.push([fb_company['calc_output'][indicator][year], fb_feed[indicator + '_median'][year]]);
                        mean_corr.push([fb_company['calc_output'][indicator][year], fb_feed[indicator + '_mean'][year]]);
                    };

                    altmanCorr = document.createElement('div');
                    altmanCorr.setAttribute('id', indicator + 'Corr');
                    altmanCorr.setAttribute('class', 'corr-chart');
                    chartRow.appendChild(altmanCorr);
                    chart = new ApexCharts(altmanCorr, getOptionScatterChart(mean_corr, med_corr));
                    chart.render();

                    newTbl = document.createElement('table');
                    formRow.appendChild(newTbl);
                    newTbl.setAttribute('class', 'indic-table');
                    newTbd = document.createElement('tbody');
                    newTbl.appendChild(newTbd)

                    newRow = document.createElement('tr');
                    newCol = document.createElement('td');
                    newCol.innerHTML = 'Rating'
                    newRow.appendChild(newCol)
                    newCol = document.createElement('td');
                    newCol.setAttribute('class', 'td-number');
                    newCol.innerHTML = String(Math.round(10000 * ratio) / 100) + '%'
                    newCol.setAttribute('name', gradeRatio(ratio))
                    newRow.appendChild(newCol)
                    newTbd.append(newRow)

                    newRow = document.createElement('tr');
                    newCol = document.createElement('td');
                    newCol.innerHTML = 'Korelacia s konkurenciou'
                    newRow.appendChild(newCol)
                    newCol = document.createElement('td');
                    newCol.setAttribute('class', 'td-number');
                    newCol.innerHTML = Math.round(10000 * pcorr(Object.values(fb_company['calc_output'][indicator]), Object.values(fb_feed[indicator + '_median']))) / 10000
                    newRow.appendChild(newCol)
                    newTbd.append(newRow)

                    newRow = document.createElement('tr');
                    newCol = document.createElement('td');
                    newCol.innerHTML = 'Odkylka vzorky v roku'
                    newRow.appendChild(newCol)
                    newCol = document.createElement('td');
                    newCol.setAttribute('class', 'td-number');
                    newCol.innerHTML = std
                    newRow.appendChild(newCol)
                    newTbd.append(newRow)

                    newRow = document.createElement('tr');
                    newCol = document.createElement('td');
                    newCol.innerHTML = 'Priemer vzorky v roku'
                    newRow.appendChild(newCol)
                    newCol = document.createElement('td');
                    newCol.setAttribute('class', 'td-number');
                    newCol.innerHTML = mean
                    newRow.appendChild(newCol)
                    newTbd.append(newRow)

                    newRow = document.createElement('tr');
                    newCol = document.createElement('td');
                    newCol.innerHTML = 'Ukazovatel podniku v roku'
                    newRow.appendChild(newCol)
                    newCol = document.createElement('td');
                    newCol.setAttribute('class', 'td-number');
                    newCol.innerHTML = comp
                    newRow.appendChild(newCol)
                    newTbd.append(newRow)
                }   
            }

            console.log(seriesData)
            console.log(labelsData)
            
            radialChartRating.innerHTML = ""
            chart = new ApexCharts(radialChartRating, getRadialChartOptions(seriesData, labelsData));
            chart.render();
            
            document.getElementById('searchingStatus').style.display = 'none';
        })
    })

}