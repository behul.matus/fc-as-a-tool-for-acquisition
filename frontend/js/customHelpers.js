let roller = true;

const pcorr = (x, y) => {
    let sumX = 0,
        sumY = 0,
        sumXY = 0,
        sumX2 = 0,
        sumY2 = 0;
    const minLength = x.length = y.length = Math.min(x.length, y.length),
        reduce = (xi, idx) => {
        const yi = y[idx];
        sumX += xi;
        sumY += yi;
        sumXY += xi * yi;
        sumX2 += xi * xi;
        sumY2 += yi * yi;
        }
    x.forEach(reduce);
    return (minLength * sumXY - sumX * sumY) / Math.sqrt((minLength * sumX2 - sumX * sumX) * (minLength * sumY2 - sumY * sumY));
};

function sortObjectByKeys(o) {
    return Object.keys(o).sort().reduce((r, k) => (r[k] = o[k], r), {});
}

function rollHead() {
    if (roller) {
        document.getElementById('searcherWrapper').removeAttribute('name')
        document.getElementById('searcherWrapper').setAttribute('name', 'hidden')
        document.getElementById('rollback').removeAttribute('name')
        document.getElementById('rollback').setAttribute('name', 'rollhide')
        document.getElementById('rollclick').removeAttribute('name')
        document.getElementById('rollclick').setAttribute('name', 'clickhide')
        document.getElementById('btcanvas').removeAttribute('name')
        document.getElementById('btcanvas').setAttribute('name', 'canvashide')
        roller = false;
    } else {
        document.getElementById('searcherWrapper').removeAttribute('name')
        document.getElementById('searcherWrapper').setAttribute('name', 'shown')
        document.getElementById('rollback').removeAttribute('name')
        document.getElementById('rollback').setAttribute('name', 'rollshow')
        document.getElementById('rollclick').removeAttribute('name')
        document.getElementById('rollclick').setAttribute('name', 'clickshow')
        document.getElementById('btcanvas').removeAttribute('name')
        document.getElementById('btcanvas').setAttribute('name', 'canvasshow')
        roller = true;
        
    }
}

function getOptionLineChart(podnik_values, priemer_values, median_values, podnik_years, series_name) {
    return {
        colors: ['#FAFF00', '#0687FF', '#5CBF66'],
        series: [
            {
                name: "Podnik",
                data: podnik_values
            },
            {
                name: series_name + ' priemer',
                data: priemer_values
            },
            {
                name: series_name + ' medián',
                data: median_values
            }
        ],
        chart: {
            height: 350,
            // width: 600,
            type: 'line',
            zoom: {
                enabled: false
            },
            toolbar: {
                show: false
            },
            background: '#434343'
        },
        legend: {
            labels: {
                colors: '#ffffff',
            },
            fontSize: '16px'
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: 'smooth'
        },
        title:{
            show: false
        },
        tooltip: {
            theme: "dark",
            style: {
                fontSize: '16px',
            }
        },
        xaxis: {
            categories: podnik_years,
            labels: {
                style: {
                    colors: "#ffffff",
                    fontSize: '16px',
                }
            },
            decimalsInFloat: 4
        },
        yaxis: {
            labels: {
                style: {
                    colors: "#ffffff",
                    fontSize: '16px',
                }
            },
            decimalsInFloat: 2
        }
    };
}

function getOptionScatterChart(mean_corr, med_corr) {
    return {
        series: [
            {
                name: "Korelácie s priemerom",
                data: mean_corr
            },
            {
                name: "Korelácia s mediánom",
                data: med_corr
            }
        ],
        tooltip: {
            theme: "dark",
            style: {
                fontSize: '16px',
            }
        },
        legend: {
            labels: {
                colors: '#ffffff',
            },
            fontSize: '16px'
        },
        chart: {
            height: 350,
            // width: 650,
            type: 'scatter',
            zoom: {
                enabled: false,
            },
            toolbar: {
                show: false
            },
        },
        xaxis: {
            tickAmount: 10,
            labels: {
                formatter: function(val) {
                    return parseFloat(val).toFixed(2)
                },
                style: {
                    colors: "#ffffff",
                    fontSize: '16px'
                }
            },
            decimalsInFloat: 2,
            title: {
                show: true,
                text: "Podnik",
                offsetY: -20,
                style: {
                    color: '#ffffff',
                    fontSize: '16px',
                    fontWeight: 100, 
                    cssClass: 'apexcharts-axis-label',
                }
            }
        },
        yaxis: {
            tickAmount: 7,
            labels: {
                style: {
                    colors: "#ffffff",
                    fontSize: '16px'
                }
            },
            decimalsInFloat: 2,
            title: {
                show: true,
                text: "Medián / Priemer",
                // offsetX: 10,
                style: {
                    color: '#ffffff',
                    fontSize: '16px',
                    fontWeight: 100,
                    cssClass: 'apexcharts-axis-label',
                }
            }
        }
    }
}

const translateIndicator = {
    "aktiva_na_zavazkoch": ["Aktíva na záväzkoch", 1], 
    "altmanovo_z_score": ["Altmanovo Z skóre", 1], 
    // "cash_flow_i": ["Cash Flow I", 1], 
    // "cash_flow_ii": ["Cash Flow II", 1], 
    // "cash_flow_iii": ["Cash Flow III", 1], 
    "cash_flow_na_vynosoch": ["Cash Flow na výnosoch", 1], 
    "cash_flow_na_zavazkoch": ["Cash Flow na záväzkoch", 1], 
    "cisty_pracovny_kapital_na_aktivach": ["Čistý pracovný kapitál na aktívach", 1], 
    // "doba_inkasa_pohladavok": ["Doba inkasa pohľadávok", -1], 
    // "doba_obratu_zasob": ["Doba obratu zásob", -1], 
    // "doba_splatnosti_zavazkov": ["Doba splatnosti záväzkov", -1], 
    "ebit_na_aktivach": ["EBIT na aktívach", 1], 
    "ebit_na_kratkodobych_zavazkoch": ["EBIT na krátkodobých záväzkoch", 1], 
    "ebit_na_trzbach": ["EBIT na tržbách", 1], 
    "index_bonity": ["Index bonity", 1], 
    "kralickov_rychly_test": ["Králičkov rýchly test", 1],
    "likvidita_1": ["Likvidita L1", 1], 
    "likvidita_2": ["Likvidita L2", 1], 
    "likvidita_3": ["Likvidita L3", 1], 
    "materialova_nakladovost": ["Materiálová nákladovosť", 1], 
    "navratnost_cudzieho_kapitalu": ["Návratnosť cudzieho kapitálu", 1], 
    "nerozdeleny_zisk_na_aktivach": ["Nerozdelený zisk na aktívach", 1], 
    "obrat_aktiv": ["Obrat aktív", 1], 
    "prevadzkova_nakladovost": ["Prevádzková nákladovosť", 1], 
    "pridana_hodnota_na_trzbach": ["Pridaná hodnota na tržbách", 1], 
    "rentabilita_aktiv": ["Rentabilita aktív", 1], 
    "rentabilita_trzieb": ["Rentabilita tržieb", 1], 
    "rentabilita_vlastneho_kapitalu": ["Rentabilita vlastného kapitálu", 1], 
    "springate_model": ["Springate model", 1], 
    "stupen_samofinancovania": ["Stupeň samofinancovania", 1], 
    "tokove_zadlzenie": ["Tokové zadĺženie", 1], 
    "trzby_na_aktivach": ["Tržby na aktívach", 1], 
    // "uverova_zadlzenost_aktiv": ["Úverová zadĺženosť aktív", -1], 
    "vlastne_imanie_na_zavazkoch": ["Vlastné imanie na záväzkoch", 1], 
    // "zasoby_na_trzbach": ["Zásoby na tržbách", -1], 
    "zlate_bilancne_pravidlo": ["Zlaté bilančné pravidlo", 1], 
}


function gradeRatio(ratio) {
    if (ratio > 2.0) {
        return "A+++"
    } else if (ratio > 1.5) {
        return "A++"
    } else if (ratio > 1.0) {
        return "A+"
    } else if (ratio > 0.9) {
        return "A"
    } else if (ratio > 0.8) {
        return "B"
    } else if (ratio > 0.7) {
        return "C"
    } else if (ratio > 0.6) {
        return "D"
    } else if (ratio > 0.5) {
        return "E"
    } else if (ratio < 0.5) {
        return "F"
    }
}

function getRadialChartOptions(seriesData, labelsData) {
    return {
        series: seriesData,
        chart: {
        // height: 350,
        type: 'radialBar',
      },
      plotOptions: {
        radialBar: {
          dataLabels: {
            name: {
              fontSize: '22px',
            },
            value: {
                color: "#ffffff",
                fontSize: "16px",
                show: true, 
                formatter: function (val) {
                    return String(Math.round(100 * val) / 100) + '%'
                }
            },
            total: {
                show: true,
                color: "#ffffff",
                label: 'Priemer',
                formatter: function (val) {
                    // return String(Math.round(100 * val) / 100) + '%'
                    // console.log(val)
                    // return 
                    return String(Math.round(100 * (val['config']['series'].reduce( ( p, c ) => p + c, 0 ) / val['config']['series'].length)) / 100) + '%'
                }
            //   formatter: function (w) {
            //     // By default this function returns the average of all series. The below is just an example to show the use of custom formatter function
            //     return 249
            //   }
            }
          }
        }
      },
      stroke: {
        lineCap: "round",
      },
      colors: ['#0DA14A'],
      labels: labelsData,
      }

    // {
    //     series: seriesData,
    //     chart: {
    //     // height: 390,
    //     type: 'radialBar',
    // },
    // plotOptions: {
    //     radialBar: {
    //     offsetY: 0,
    //     startAngle: 0,
    //     endAngle: 270,
    //     hollow: {
    //         margin: 5,
    //         size: '30%',
    //         background: 'transparent',
    //         image: undefined,
    //     },
    //     dataLabels: {
    //         name: {
    //         show: false,
    //         },
    //         value: {
    //         show: false,
    //         }
    //     }
    //     }
    // },
    // colors: ['#0DA14A'],
    // labels: labelsData,
    // legend: {
    //     show: true,
    //     floating: true,
    //     fontSize: '16px',
    //     position: 'left',
    //     height: '20vh',
    //     offsetX: 160,
    //     offsetY: 15,
    //     labels: {
    //     useSeriesColors: true,
    //     },
    //     markers: {
    //     size: 0
    //     },
    //     formatter: function(seriesName, opts) {
    //     return seriesName + ":  " + opts.w.globals.series[opts.seriesIndex]
    //     },
    //     itemMargin: {
    //     vertical: 3
    //     }
    // },
    // responsive: [{
    //     breakpoint: 480,
    //     options: {
    //     legend: {
    //         show: false
    //     }
    //     }
    // }]
    // }
}