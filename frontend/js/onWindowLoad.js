const firebaseConfig = {
    apiKey: "AIzaSyCxWvoII1SJna2VsRtRkcyQtL1xlFd7Fwo",
    authDomain: "fc-acquisition-master.firebaseapp.com",
    projectId: "fc-acquisition-master",
    storageBucket: "fc-acquisition-master.appspot.com",
    messagingSenderId: "833788116547",
    appId: "1:833788116547:web:d706aad4a6375de37bdef6",
    measurementId: "G-FYVYSBH7EJ"
};

const searchByDict = {
    'Okres' : 'okres',
    'Odvetvie' : 'odvetvie',
    'SK NACE' : 'sknace_code'
}

firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();

window.addEventListener('load', function() {
    document.getElementById('rollback').setAttribute('name', 'rollshow')
    document.getElementById('rollclick').setAttribute('name', 'clickshow')
    document.getElementById('btcanvas').setAttribute('name', 'canvasshow')
    const icoList = document.getElementById('icoList')
    const icoInput = document.getElementById('icoInput');
    const yearList = document.getElementById('yearList');
    const byList = document.getElementById('byList');
    const companyName = document.getElementById('companyName');
    const companySKNACE = document.getElementById('companySKNACE');
    const companyOdvetvie = document.getElementById('companyOdvetvie');
    const companyOkres = document.getElementById('companyOkres');
    const companyProfile = document.getElementById('companyProfile');
    const evaluateBy = document.getElementById('byId');
    const radialChartRating = document.getElementById("radialChart")

    db.collection('config').doc('companies').get().then((fb_doc) => {
        fb_doc = fb_doc.data()
        for (i in fb_doc) {
            
            let newOpt = document.createElement('option');
            newOpt.setAttribute('value', i);
            icoList.appendChild(newOpt);
        }
    })

    icoInput.addEventListener('change', function() {
        db.collection('comp_list').doc(icoInput.value).get().then((fb_company) => {
            fb_company = fb_company.data()
            yearList.innerHTML = "";
        
            for (year in fb_company['years']) {
                let newOpt = document.createElement('option');
                newOpt.setAttribute('value', fb_company['years'][year]);
                yearList.appendChild(newOpt);
            }

            companyName.innerHTML = fb_company['Názov'];
            companySKNACE.innerHTML = fb_company['SK NACE'];
            companyOdvetvie.innerHTML = fb_company['Odvetvie'];
            companyOkres.innerHTML = fb_company['Okres'];
            companyProfile.style.display = 'grid';

            byList.innerHTML = "";

            newOpt = document.createElement('option');
            newOpt.setAttribute('value', 'SK NACE');
            newOpt.innerHTML = fb_company['sknace_name'];
            byList.appendChild(newOpt);

            newOpt = document.createElement('option');
            newOpt.setAttribute('value', 'Odvetvie');
            newOpt.innerHTML = fb_company['Odvetvie'];
            byList.appendChild(newOpt);

            newOpt = document.createElement('option');
            newOpt.setAttribute('value', 'Okres');
            newOpt.innerHTML = fb_company['Okres'];
            byList.appendChild(newOpt);
        })
        
    })

    document.getElementById('searchBtn').addEventListener('click', function() {
        if (document.getElementById('icoInput').value == "") {
            window.alert('Chýba podnik!')
        } else if (document.getElementById('yearId').value == "") {
            window.alert('Chýba hodnotený rok!')
        } else if (document.getElementById('byId').value == "") {
            window.alert('Chýba údaj, podľa ktorého budeme hodnotiť!')
        } else {
            rollHead()
            createCharts(evaluateBy.value)
        }
        
    })

})