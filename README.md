# FC as a tool for Acquisition

Repozitár pre verifikáciu kódu využitého pri zistavovaní programu na získavanie a spracovávanie dát za účelom vytvorenia automatizácie a zdokonalenie finančného controllingu (resp. finančného due diligance) a jeho využitie v diplomovej práce. 

## BACKEND
>Z finstat.sk som ziskal data o firmach, ktore vykazali financne udaje za rok 2020 (zrusene aj fungujuce). Tato databaza sa ulozi do PostgreSQL a dalej je vyuzivana an accessovanie Registra UZ, pricom ziskane uctovne vykazy sa uklataju tiez do PostgreSQL (ina databaza). Pocas stahovania dat sa rovno kalkuluju zakladne suhrnne polozky vykazov a financne ukazovatele.\
>Po ziskani pozadovaneho mnozstva dat sa spusta konsolidacia udajov, teda vypocet statistickej vzorky a zapis statistickeho suboru ako aj dat pre jednotlive firmy na Firebase Firestore, odkial sa dopytuju pomocou integrovanej Firebase aplikacie (frontend). Vyuzivaju sa tu kniznice od firebase, postgres (psycopg2), pandas, sqlalchemy

## FRONTEND
>Vyuziva kniznice FIrebase na ziskanie dat a ApexCharts na vytv=aranie vizualizacii. Uzivatel si vyberie ICO, appka natiahne dostupne roky a vzorku pre hodnotenie (odvetvie a pod.) a uzivatel si musi zvolit aj rok aj vzorku pre hodnotenie. Aplikacia po zadani udajov a spusteni hodnotenia vygeneruje pre kazdy ukazovatel 2 grafy, jeden linovy v case, kde sa porovnava priemer a median vzorky s podnikom, a druhy tzv. "scatter chart" na identifikaciu korelacie podniku s priemerom a medianom. K jednotlivym ukazovatelom je zaroven uvedeny aj priemer v zvolenom roku, median, smerodajna odchylka a bodove hodnotenie, ktore je aktualne pocitane ako (podnik) / ((priemer + s.odch) - (priemer - s.odch)), vyjadrene v %. 